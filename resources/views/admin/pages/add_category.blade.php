@extends('admin.layout.app')
@section('content')
        <div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Thêm Danh Mục Sản Phẩm
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <form role="form" method="post" action="{{URL::to('/save-category')}}">
                                     @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên Danh Muc:</label>
                                    <input type="text" class="form-control" name="category_name" id="exampleInputEmail1" placeholder="Tên danh mục">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Mô tả danh mục</label>
                                    <textarea style="resize: none;" rows="5" class="form-control" id="exampleInputPassword1" placeholder="Thêm mô tả" name="category_content"></textarea>
                                </div>
                                <button type="submit" name="add_category_product" class="btn btn-info">Thêm danh mục</button><br>
                                <?php
                                $message = Session::get('message');
                                if($message){
                                    echo '<span class="text-alert" style="color:red;">'.$message.'</span>';
                                    Session::put('message',null);
                                }
                                ?>
                            </form>
                            </div>

                        </div>
                    </section>

            </div>
@endsection