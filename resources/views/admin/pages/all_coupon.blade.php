@extends('admin.layout.app')
@section('content')
  
        <div class="table-agile-info">
  <div class="panel panel-default">
    <div class="panel-heading">
      Liệt Kê Mã Giảm Giá
    </div>
    <div class="row w3-res-tb">
      <div class="col-sm-5 m-b-xs">
        <select class="input-sm form-control w-sm inline v-middle">
          <option value="0">Bulk action</option>
          <option value="1">Delete selected</option>
          <option value="2">Bulk edit</option>
          <option value="3">Export</option>
        </select>
        <button class="btn btn-sm btn-default">Apply</button>                
      </div>
      <div class="col-sm-4">
      </div>
      <div class="col-sm-3">
        <div class="input-group">
          <input type="text" class="input-sm form-control" placeholder="Search">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
        </div>
      </div>
    </div>
    <div class="table-responsive">
       <?php
                                $message = Session::get('message');
                                if($message){
                                    echo '<span class="text-alert" style="color:red;">'.$message.'</span>';
                                    Session::put('message',null);
                                }
                                ?>
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            
            <th>Tên Mã</th>
            <th>Mã giảm </th>
            <th>Số lượng</th>
            <th>Tính năng </th>
            <th>số tiền giảm , số phần trăm</th>
            <th style="width:30px;"></th>
          </tr>
        </thead>
        <tbody>
          @foreach($coupon as $key => $capo) 
          <tr>
           
            <td>{{ $capo->coupon_name }}</td>
            <td>{{ $capo->coupon_code }}</td>
            <td>{{ $capo->coupon_number }}</td>
            <td>
              <?php
              if ($capo->coupon_info ==1) {
                echo "Giảm theo phần trăm";
              }else{
                echo "Giảm theo tiền";
              }
              ?>
             
            </td>
            <td>
              <?php
                if ($capo->coupon_info ==2) {
                 echo "Giảm ".$capo->coupon_write." VND";
              }else{
                 echo "Giảm".$capo->coupon_write."%";
              }
              ?>
              </td>
            <td>
                <a href=" {{URL::to('/delete-coupon/'.$capo->coupon_id)}}  " onclick="return confirm('Bạn có chắc chắn muốn xóa mã')"><i class="fa fa-times text-danger text"></i></a>
            </td>
          </tr>      
           @endforeach
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        
        <div class="col-sm-5 text-center">
          <small class="text-muted inline m-t-sm m-b-sm">showing 20-30 of 50 items</small>
        </div>
        <div class="col-sm-7 text-right text-center-xs">                
          <ul class="pagination pagination-sm m-t-none m-b-none">
            <li><a href=""><i class="fa fa-chevron-left"></i></a></li>
            <li><a href="">1</a></li>
            <li><a href="">2</a></li>
            <li><a href="">3</a></li>
            <li><a href="">4</a></li>
            <li><a href=""><i class="fa fa-chevron-right"></i></a></li>
          </ul>
        </div>
      </div>
    </footer>
  </div>
</div>


@endsection