
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index');

Route::get('/admin', function () {
    return view('admin.layout.app');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//categorty

Route::get('add-category','CategoryController@add_category');

Route::post('/save-category','CategoryController@insert_category');

Route::get('all-category','CategoryController@all_category');

Route::get('edit-category/{cate_id}','CategoryController@edit_category');

Route::post('/update-category','CategoryController@update_category');

Route::get('delete-category/{cate_id}','CategoryController@delete_category');


//brand 
Route::get('add-brand','BrandController@add_brand');

Route::post('save-brand','BrandController@save_brand');

Route::get('all-brand','BrandController@all_brand');

Route::get('edit-brand/{brand_id}','BrandController@edit_brand');

Route::post('update-brand','BrandController@update_brand');

Route::get('delete-brand/{brand_id}','BrandController@delete_brand');

//product 
Route::get('/add-product','ProductController@add_product');

Route::post('/save-product','ProductController@save_product');

Route::get('/all-product','ProductController@all_product');

Route::get('/edit-product/{product_id}','ProductController@edit_product');

Route::post('/update-product','ProductController@update_product');

Route::get('/delete-product/{product_id}','ProductController@delete_product');


//client


Route::get('/danh-muc/{category_id}','IndexController@category'); 

Route::get('thuong-hieu/{brand_id}','IndexController@show_brand'); 

Route::post('/search','IndexController@search_product');

Route::get('/show-product/{product_id}','IndexController@show_product');


//cart

Route::post('/save-cart','CartController@save_cart');

Route::get('/show-cart','CartController@show_cart');

Route::get('/delete-cart/{rowId}','CartController@delete_cart');

Route::post('/update-quality','CartController@update_quality');
 
Route::get('/delete-all-cart','CartController@delete_all_cart');

//checkout 
Route::get('login-checkout','CheckoutController@login_checkout');

Route::post('/add-customer','CheckoutController@add_customer');

Route::post('/login-customer','CheckoutController@login_customer');

Route::get('/write-shipping','CheckoutController@write_shipping');

Route::get('/logout-checkout','CheckoutController@logout_checkout');

Route::post('/save-checkout-customer','CheckoutController@save_checkout');

Route::get('/payment','CheckoutController@show_payment');

Route::post('/order-place','CheckoutController@order_place');

Route::get('/end-payment','CheckoutController@end_payment');

Route::get('/show-order','OrderController@show_order');

Route::get('/show-chitiet/{order_id}','OrderController@show_chitiet');


//facebook
Route::get('/login-facebook/{provider}','Auth\LoginController@redirect');
Route::get('/login/callback/{provider}','Auth\LoginController@callback');


Route::post('/test-vnpay','CheckoutController@test_vnpay');

//coupon 

Route::get('/add-coupon','CouponController@add_coupon');

Route::post('/save-coupon','CouponController@save_coupon');

Route::get('/all-coupon','CouponController@all_coupon');
Route::get('/delete-coupon/{coupon_id}','CouponController@delete_coupon');

Route::post('/check-coupon','CartController@check_coupon');