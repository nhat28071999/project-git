@extends('client.layout.cart_app')
@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Hình thức thanh toán</li>
				</ol>
			</div><!--/breadcrums-->
			<div class="review-payment">
				<h2>Sản phẩm đã chọn</h2>
				<div class="table-responsive cart_info">
				<?php
					$content=Cart::content();
				?>
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Hình ảnh</td>
							<td class="description">Mô tả</td>
							<td class="price">Gía</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Tổng tiền</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@foreach($content as $content_cart)
						<tr>
							<td class="cart_product">
								<a href=""><img src="upload\product\{{$content_cart ->options->image}}" alt="" width="70"></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{ $content_cart->name}}</a></h4>
								<p>{{ $content_cart->id}}</p>
							</td>
							<td class="cart_price">
								<p>{{ number_format($content_cart->price)."VND"}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<form action="{{URL::to('/update-quality')}}" method="POST">
										{{csrf_field() }}
									<input class="cart_quantity_input" type="text" name="quantity" value="{{ $content_cart->qty}}" autocomplete="off" size="2">
									<input type="hidden" value="{{$content_cart->rowId}}" name="rowid_update">
									<input type="submit" name="update" value="Cập nhật">
									</form>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">
									<?php
									$subtotal = $content_cart->price * $content_cart->qty;
									echo number_format($subtotal)."VND" ; 
									?>
								</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="{{URL::to('/delete-cart/'.$content_cart->rowId)}}"><i class="fa fa-times"></i></a>
							</td>
						</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			</div>

			

			<h4 style="margin: 40px 0px; font-size: 20px;">Chọn hình thức thanh toán:</h4>
			<p>Thanh toán bằng thẻ :</p>
			<div id="paypal-button"></div><br><br>
			<form action="{{URL::to('/order-place')}}" method="POST">
				{{ csrf_field() }}
			<div class="payment-options">
					<span>
						<label><input type="checkbox" value="1" name="payment_option">Đã thanh toán bằng ATM</label>
						<label><input type="checkbox" value="2" name="payment_option"> Thanh toán bằng tiền mặt</label>
					</span>
					<input type="submit" value="Đặt hàng" name="send_other" class="btn btn-primary btn-sm">
				</div>
			</form>
		</div>
	</section> <!--/#cart_ite
@endsection