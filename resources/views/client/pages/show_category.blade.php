
@extends('client.layout.app')
@section('content')
<div class="features_items"><!--features_items-->
                        <h2 class="title text-center">Sản phẩm nffvổi bật</h2>
                        @foreach($product as $key  => $pro)
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="..\upload\product\{{$pro->product_image}}" alt="" />
                                            <h2>{{ number_format($pro->product_price).'VNĐ'}}</h2>
                                            <p>{{ $pro->product_name}}</p>
                                             <form action="{{ URL::to('/save-cart')}}" method="POST">
                                                 {{csrf_field() }}
                                                 <input type="hidden" name="qly" min="1" value="1" />
                                                <input type="hidden" name="product_hidden" value="{{ $pro->product_id }}" />
                                                <button type="submit" class="btn btn-fefault cart">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Thêm vào giỏ hàng
                                                </button>
                                            </form>
                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">
                                                <h2>{{ number_format($pro->product_price).'VNĐ'}}</h2>
                                                <p>{{ $pro->product_name}}</p>
                                                 <form action="{{ URL::to('/save-cart')}}" method="POST">
                                                 {{csrf_field() }}
                                                 <input type="hidden" name="qly" min="1" value="1" />
                                                <input type="hidden" name="product_hidden" value="{{ $pro->product_id }}" />
                                                <button type="submit" class="btn btn-fefault cart">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Thêm vào giỏ hàng
                                                </button>
                                            </form>
                                            </div>
                                        </div>
                                </div>
                                <div class="choose">
                                    <ul class="nav nav-pills nav-justified">
                                        <li><a href="{{ URL::to('/chitietsanpham/'.$pro->product_id)}}"><i class="fa fa-plus-square"></i>Chi tiết sản phẩm</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach
  @endsection