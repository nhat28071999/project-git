<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Cart;
use App\Rules\Captcha; 
use Mail;
session_start();

class CheckoutController extends Controller
{
    public function login_checkout(){
    	return view('client.pages.login-checkout');
    }

    public function add_customer(Request $request){
    	$request->validate([
    		'customer_name' => 'required',
    		'customer_email' =>'required',
    		'customer_password' => 'required',
    		'customer_phone' => 'required',
            'g-recaptcha-response' => new Captcha(),     
    	],[
    		'customer_name.required' => 'username không được rỗng ',
    		'customer_email.required' =>'emal không được rỗng',
    		'customer_password.required' => 'password không được rỗng',
    		'customer_phone.required' => 'phone không được rỗng'
    		]);
    	
    	$data = array();
    	$data['user_name'] = $request->customer_name;
    	$data['user_email'] = $request->customer_email;
    	$data['user_password'] = md5($request->customer_password);
    	$data['user_phone'] = $request->customer_phone;

    	$result = DB::table('user')->insert($data);
    	Session::put('done','Đăng ký thành công');
    	return Redirect::to('/login-checkout');
    }

    public function login_customer(Request $request){
    	$request->validate([
    		'email_accout' => 'required',
    		'password_accout' => 'required'
    	],[
    		'email_accout.required' => 'Tên đăng nhập không được rỗng',
    		'password_accout.required' => 'Password không được rỗng'

    	]);

    	$email = $request->email_accout;
    	$password = md5($request->password_accout);

    	$result = DB::table('user')->where('user_name',$email)->where('user_password',$password)->first();
    	if ($result){
    		Session::put('customer_id',$result->user_id);
    		return Redirect::to('/write-shipping');
    	}else{
    		Session::put('done1','Tài khoản hoặc mật khẩu không đúng');
    		return Redirect::to('/login-checkout');
    	}
    		}


    	public function logout_checkout(){
    		Session::flush();
    		return Redirect::to('/login-checkout');
    	}




    	public function write_shipping(){
    		return view('client.pages.checkout');
    	}

    	public function save_checkout(Request $request){
    		$data = array();
    		$data['shipping_email'] = $request->shipping_email;
    		$data['shipping_name'] = $request->shipping_name;
    		$data['shipping_phone'] = $request->shipping_phone;
    		$data['shipping_address'] = $request->shipping_address;
    		$data['shipping_notes'] = $request->shipping_notes;

           
            Mail::send('client.pages.send-mail',[
                'name' => "Đặt hàng thành công",
                'body' => "Cảm ơn quý khách đã mua hàng đã mua hàng ",
            ],function($mail) use($request){
                $mail->to($request->shipping_email);
                $mail->from('nhat28071999@gmail.com');
                $mail->subject('Thông báo đặt hàng');
            });



    		$result = DB::table('tbl_shipping')->insertGetId($data);
    		Session::put('shipping_id',$result);
    		return Redirect::to('/payment');
    	}

        public function show_payment(){
            return view('client.pages.payment');
        }

        public function order_place(Request $request){
            $place = $request->payment_option;
            //payment
            $data['payment_method'] = $request->payment_option;
            $data['payment_status'] = 'Đang xử lý';

            $payment = DB::table('tbl_payment')->insertGetId($data);

            //order 
            $dataorder['user_id'] = Session::get('customer_id'); 
            $dataorder['shipping_id'] = Session::get('shipping_id');
            $dataorder['payment_id'] = $payment;
            $dataorder['order_total'] = Cart::total();
            $dataorder['order_status'] = 'Đang xử lý';

            $order = DB::table('tbl_order')->insertGetId($dataorder);

            //order_detail
            $content=Cart::content();
            foreach($content as $content_cart){
                $dataorderdetail['order_id'] = $order;
                $dataorderdetail['product_id'] = $content_cart->id;
                $dataorderdetail['product_name'] = $content_cart->name;
                $dataorderdetail['product_price'] = $content_cart->price;
                $dataorderdetail['product_sales_quantity'] = $content_cart->qty;


            $order_detail = DB::table('tbl_order_detail')->insert($dataorderdetail);
            }

        $moneypayment = $dataorderdetail['product_price']*$dataorderdetail['product_sales_quantity'];

            
            if ($place == 1 ) {
                Cart::destroy();
                Session::put('money', $moneypayment );
                return Redirect::to('/end-payment');
            }elseif($place ==2){
                Cart::destroy();
                Session::put('message','Cảm ơn bạn đã mua hàng , sản phẩm sẽ được giao sớm nhất đến bạn');
                return Redirect::to('/end-payment');
            }
        }

        public function end_payment(){
            return view('client.pages.end-payment');
        }

       public function test_vnpay(Request $request)
    {
        $money = $request->money;
        session(['cost_id' => $request->id]);
        session(['url_prev' => url()->previous()]);
        $vnp_TmnCode = "3KRP90QD"; //Mã website tại VNPAY 
        $vnp_HashSecret = "LMFYNZYSFPPZTFXNLPTKDGNMZZKURZEZ"; //Chuỗi bí mật
        $vnp_Url = "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
        $vnp_Returnurl = "http://localhost/blog/public/payment";
        $vnp_TxnRef = date("YmdHis"); //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = "Thanh toán hóa đơn phí dich vụ";
        $vnp_OrderType = 'billpayment';
        $vnp_Amount =  $money* 100 ;
        $vnp_Locale = 'vn';
        $vnp_IpAddr = request()->ip();

        $inputData = array(
            "vnp_Version" => "2.0.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . $key . "=" . $value;
            } else {
                $hashdata .= $key . "=" . $value;
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
           // $vnpSecureHash = md5($vnp_HashSecret . $hashdata);
            $vnpSecureHash = hash('sha256', $vnp_HashSecret . $hashdata);
            $vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
        }
        return redirect($vnp_Url);
    }

    }

