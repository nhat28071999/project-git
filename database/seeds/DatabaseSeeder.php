<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
      'name' => 'admin',
      'email' => 'nhat28071999@gmail.com',
      'password' => bcrypt('Nhat123456'),
]);
    }
}
