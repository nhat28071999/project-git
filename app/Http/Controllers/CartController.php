<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Cart;
use App\coupon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
session_start();

class CartController extends Controller
{
    public function save_cart(Request $request){
    	$product_id = $request->product_hidden;
    	$qly = $request->qly;

    	$result = DB::table('tbl_product')->where('product_id',$product_id)->first();

    	$data = array();
    	$data['id'] = $product_id ;
    	$data['name'] = $result->product_name;
    	$data['qty'] = $qly;
    	$data['price'] = $result->product_price;
    	$data['weight'] = 112;
    	$data['options']['image'] = $result->product_image;

    	Cart::add($data);
    	return Redirect::to('/show-cart');
    }

    public function show_cart(){
    	$category = DB::table('tbl_category')->orderby('category_id','desc')->get();
    	$brand =DB::table('tbl_brand')->orderby('brand_id','desc')->get();

    	return view('client.pages.show_cart')->with('category',$category)->with('brand',$brand);
    }

    public function delete_cart($row_Id){
    	Cart::update($row_Id, 0);
    	return Redirect::to('/show-cart');
    }

    public function delete_all_cart(){
        Session::flush(Cart::content());
        Session::put('message','Xóa hết tất cả sản phẩm thành công');
        return Redirect::to('/show-cart');
    }

    public function update_quality(Request $request){
    	$rowid = $request->rowid_update;
    	$quality = $request->quantity;

    	Cart::update($rowid,$quality);
    	return Redirect::to('/show-cart');
    }

    public function check_coupon(Request $request){
            $data = $request->all();
            $coupon = coupon::where('coupon_code',$data['coupon_code'])->first();
            if ($coupon) {
                $count_coupon = $coupon->count();
                if ($count_coupon > 0 ) {
                    $coupon_session = Session::get('coupon');
                    if ($coupon_session == true) {
                        $is_avaible = 0;
                        if ($is_avaible ==0) {
                           $cou[] = array(
                                'coupon_code' => $coupon->coupon_code,
                                'coupon_info' => $coupon->coupon_info,
                                'coupon_write' => $coupon->coupon_write,
                           );
                           Session::put('coupon',$cou);
                        }
                    }else{
                        $cou[] = array(
                                'coupon_code' => $coupon->coupon_code,
                                'coupon_info' => $coupon->coupon_info,
                                'coupon_write' => $coupon->coupon_write,
                           );
                         Session::put('coupon',$cou);
                    }
                     Session::save();
                     return Redirect()->back()->with('message','Thêm mã giảm giá thành công');
            }
    }else{
         return Redirect()->back()->with('error','Mã giảm giá không tồn tại');
    }
}
}
