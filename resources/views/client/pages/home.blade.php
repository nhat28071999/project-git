
@extends('client.layout.app')
@section('content')
<div class="features_items"><!--features_items-->
                        <div class="fb-share-button" data-href="http://localhost/blog/public/" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flocalhost%2Fblog%2Fpublic%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                        <h2 class="title text-center">Sản phẩm nổi bật</h2>
                        @foreach($product as $key  => $pro)
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="upload\product\{{$pro->product_image}}" alt="" height="190px" width="230px" />
                                            <h2>{{ number_format($pro->product_price).'VNĐ'}}</h2>
                                            <p>{{ $pro->product_name}}</p>
                                            <form action="{{ URL::to('/save-cart')}}" method="POST">
                                                 {{csrf_field() }}
                                                 <input type="hidden" name="qly" min="1" value="1" />
                                                <input type="hidden" name="product_hidden" value="{{ $pro->product_id }}" />
                                                <button type="submit" class="btn btn-fefault cart">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Thêm vào giỏ hàng
                                                </button>
                                            </form>
                                           
                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">
                                                <h2>{{ number_format($pro->product_price).'VNĐ'}}</h2>
                                                <p>{{ $pro->product_name}}</p>
                                                <form action="{{ URL::to('/save-cart')}}" method="POST">
                                                 {{csrf_field() }}
                                                 <input type="hidden" name="qly" min="1" value="1" />
                                                <input type="hidden" name="product_hidden" value="{{ $pro->product_id }}" />
                                                <button type="submit" class="btn btn-fefault cart">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Thêm vào giỏ hàng
                                                </button>
                                            </form>
                                            </div>
                                        </div>
                                </div>
                                <div class="choose">
                                    <ul class="nav nav-pills nav-justified">
                                        <li><a href="{{ URL::to('/show-product/'.$pro->product_id)}}"><i class="fa fa-plus-square"></i>Chi tiết sản phẩm</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>

                        @endforeach
                         </div>
                        <div class="recommended_items" ><!--recommended_items-->
                        <h2 class="title text-center">Sản phẩm gợi ý</h2>
                        
                     <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                 
                                <div class="item active"> 
                                     @foreach($randompro as $key => $random) 
                                    <div class="col-sm-4">
                                       
                                        <div class="product-image-wrapper">
                                            <div class="single-products">
                                                <div class="productinfo text-center">
                                                    <img src="upload\product\{{$random->product_image}}"
                                                    style=" width :190px; height: 140px;" alt="" />
                                                    <h2>{{ number_format($random->product_price).'VND'}} </h2>
                                                    <p>{{ $random->product_name}} </p>
                                                    <form action="{{ URL::to('/save-cart')}}" method="POST">
                                                 {{csrf_field() }}
                                                 <input type="hidden" name="qly" min="1" value="1" />
                                                <input type="hidden" name="product_hidden" value="{{ $random->product_id }}" />
                                                <button type="submit" class="btn btn-fefault cart">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Thêm vào giỏ hàng
                                                </button>
                                            </form>
                                                </div>
                                            </div>
                                        </div>
                                           
                                    </div>
                                   @endforeach
                                </div>
                              
                                <div class="item"> 
                                 @foreach($randompro as $key => $random)  
                                    <div class="col-sm-4">
                                        <div class="product-image-wrapper">
                                            <div class="single-products">
                                                <div class="productinfo text-center">
                                                    <img src="upload\product\{{$random->product_image}}" style=" width :190px; height: 140px;" alt="" />
                                                    <h2>{{ number_format($random->product_price).'VND'}}</h2>
                                                    <p>{{ $random->product_name}}</p>
                                                   <form action="{{ URL::to('/save-cart')}}" method="POST">
                                                 {{csrf_field() }}
                                                 <input type="hidden" name="qly" min="1" value="1" />
                                                <input type="hidden" name="product_hidden" value="{{ $random->product_id }}" />
                                                <button type="submit" class="btn btn-fefault cart">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Thêm vào giỏ hàng
                                                </button>
                                            </form>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                 @endforeach
                            </div>
                              
                             </div>
                             <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                              </a>
                              <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                              </a>          
                        
                    </div>
                </div>

                
 
  @endsection


  <form >
                                   
                    