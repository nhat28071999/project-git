@extends('client.layout.cart_app')
@section('content')
<section id="cart_items">
		
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			 <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<div class="alert alert-success"  style="width=20%;">'.$message.'</div>';
                                Session::put('message',null);
                            }
                             $error = Session::get('error');
                            if($error){
                                echo '<div class="alert alert-danger"  style="width=20%;">'.$error.'</div>';
                                Session::put('error',null);
                            }
                            ?>
             <div>
					<a class="cart_quantity_delete" href="{{URL::to('/delete-all-cart')}}"><i class="fa fa-times"></i>Xóa tất cả sản phẩm </a>
				</div>
			<div class="table-responsive cart_info">
				<?php
					$content=Cart::content();
				?>
				
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Hình ảnh</td>
							<td class="description">Mô tả</td>
							<td class="price">Gía</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Tổng tiền</td>
							<td></td>
						</tr>
						
					</thead>
					<tbody>
						@foreach($content as $content_cart)
						<tr>
							<td class="cart_product">
								<a href=""><img src="upload\product\{{$content_cart ->options->image}}" alt="" width="70"></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{ $content_cart->name}}</a></h4>
								<p>{{ $content_cart->id}}</p>
							</td>
							<td class="cart_price">
								<p>{{ number_format($content_cart->price)."VND"}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<form action="{{URL::to('/update-quality')}}" method="POST">
										{{csrf_field() }}
									<input class="cart_quantity_input" type="text" name="quantity" value="{{ $content_cart->qty}}" autocomplete="off" size="2">
									<input type="hidden" value="{{$content_cart->rowId}}" name="rowid_update">
									<input type="submit" name="update" value="Cập nhật">
									</form>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">
									<?php
									$subtotal = $content_cart->price * $content_cart->qty;
									echo number_format($subtotal)."VND" ; 
									?>
								</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="{{URL::to('/delete-cart/'.$content_cart->rowId)}}"><i class="fa fa-times"></i></a>
							</td>
						</tr>

						@endforeach

					</tbody>
				</table>
			</div>
				
				<div>

					<form action="{{URL::to('/check-coupon')}}" method="POST">
						{{ csrf_field() }}
					<input type="text" placeholder="Nhập mã giảm giá" name="coupon_code">
					<input type="submit" value="Tính mã giảm" class="btn btn-success check-coupon">
					
					</form>
					
					
                    
				</div>
		
	</section> 
	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>Bạn có muốn tiếp tục?</h3>
				<p>Chọn xem bạn có mã giảm giá hoặc điểm thưởng muốn sử dụng hoặc muốn ước tính chi phí giao hàng của mình. </p>
			</div>
			<div class="row">
				
				<div class="col-sm-5">
					@if($content != null)
					<div class="total_area">
						<ul>
							<li>Tổng <span>{{Cart::priceTotal()}}</span></li>
							 
								@if(Session::get('coupon'))
									@foreach(Session::get('coupon') as $key => $cou)
										@if($cou['coupon_info'] == 1)
										<li>Mã giảm:
											<span>
												 {{ number_format($cou['coupon_write'])}} %

											</span></li>
											
											<p>
												@php
													$tong = str_replace(',', '',Cart::priceTotal());
													$total_coupon = $tong*$cou['coupon_write']/100;
													echo "<li>Số tiền giảm:<span>".number_format($total_coupon)."VNĐ</li></span>";
													$thanhtien =  number_format($tong-$total_coupon)
												@endphp
											</p>
											<li>
												Tổng tiền còn lại : <span> {{ $thanhtien }} VNĐ </span>
											</li>
								@elseif($cou['coupon_info'] == 2)
									<li>Mã giảm:<span>
										{{ $cou['coupon_write'] }}VNĐ
									</span></li>

										<p>
												@php
													$tong = str_replace(',', '',Cart::priceTotal());
													$total_coupon = $cou['coupon_write'];
													echo "<li>Số tiền giảm:<span>".number_format($cou['coupon_write'])."VNĐ</li></span>";
													$thanhtien =  number_format($tong-$total_coupon)
												@endphp
										</p>
										<li>
												Tổng tiền còn lại : <span> {{ $thanhtien }} VNĐ </span>
											</li>
										
										@endif
										<li>Phí vận chuyển <span>Free</span></li>
										<li>Thành tiền <span>{{ $thanhtien }} VNĐ </span></li>
									@endforeach
									@else



									<a href="#"></a>

							
							
					
							<li>Phí vận chuyển <span>Free</span></li>
							<li>Thành tiền <span>{{Cart::total()}}</span></li> 
							@endif
						</ul>

							 <?php
                                $custome = Session::get('customer_id');
                                if ($custome != NULL) { 
                                ?>
                                    <a class="btn btn-default check_out" href="{{URL::to('/write-shipping')}}">Thanh toán</a>
                                <?php
                                 }else{ 
                                ?>
                                    <a class="btn btn-default check_out" href="{{URL::to('/login-checkout')}}">Thanh toán</a>
                                <?php
                                } 
                                ?>
							
					</div>
					@elseif( $content ==null)
								@php
									Session::flush('coupon');
								@endphp
							@endif
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
	@endsection