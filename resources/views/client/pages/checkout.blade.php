@extends('client.layout.cart_app')
@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Thanh toán</li>
				</ol>
			</div><!--/breadcrums-->

		

			<div class="register-req">
				<p>Đăng ký đăng nhập để thanh toán giỏ hàng</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-6">
						<div class="shopper-info">
							<p>Thông tin địa chỉ</p>
							<form action="{{URL::to('/save-checkout-customer')}}" method="POST">
								{{ csrf_field() }}
								<input type="text" placeholder="Email" name="shipping_email">
								<input type="text" placeholder="Họ tên"  name="shipping_name">
								<input type="text" placeholder="Số điện thoại"  name="shipping_phone">
								<input type="text" placeholder="Địa chỉ"  name="shipping_address">
								<textarea  name="shipping_notes"  placeholder="Ghi chú đơn hàng của bạn" rows="10"></textarea>
								<input type="submit" value="Gửi" name="send_other" class="btn btn-primary btn-sm">
							</form>
							
						</div>
					</div>
					
								
				</div>
			</div>
			

			
			
		</div>
	</section> <!--/#cart_ite
@endsection