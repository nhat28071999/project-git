@extends('admin.layout.app')
@section('content')
        <div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Thêm mã giảm giá
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <form role="form" method="post" action="{{URL::to('/save-coupon')}}">
                                     @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên Mã:</label>
                                    <input type="text" class="form-control" name="coupon_name" id="exampleInputEmail1" placeholder="Tên danh mục">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mã giảm:</label>
                                    <input type="text" class="form-control" name="coupon_code" id="exampleInputEmail1" placeholder="Tên danh mục">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số lượng:</label>
                                    <input type="text" class="form-control" name="coupon_number" id="exampleInputEmail1" placeholder="Tên danh mục">
                                </div><div class="form-group">
                                    <label for="exampleInputEmail1">Tính năng:</label>
                                    <select name="coupon_info" class="form-control input-sm m-bot15">
                                        <option value="0"> --Chọn--</option>
                                        <option value="1">Giảm theo %</option>
                                        <option value="2">Giảm theo tiền</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số tiền giảm , số phần trăm:</label>
                                    <input type="text" class="form-control" name="coupon_write" id="exampleInputEmail1" placeholder="Tên danh mục">
                                </div>
                                <button type="submit" name="add_category_product" class="btn btn-info">Thêm Mã</button><br>
                                <?php
                                $message = Session::get('message');
                                if($message){
                                    echo '<span class="text-alert" style="color:red;">'.$message.'</span>';
                                    Session::put('message',null);
                                }
                                ?>
                            </form>
                            </div>

                        </div>
                    </section>

            </div>
@endsection