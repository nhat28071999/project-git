<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
session_start();

class CategoryController extends Controller
{
	  public function __construct()
    {
        $this->middleware('auth');
    }
	
    

    public function add_category(){
    	return view('admin.pages.add_category');
    }

    public function insert_category(Request $request){
    	$data = array();
    	$data['category_name'] = $request->category_name;
    	$data['category_content'] = $request->category_content;

    	$inser_data = DB::table('tbl_category')->insert($data);

    	Session::put('message','Thêm danh mục thành công');
    	return Redirect::to('add-category');

    }

    public function all_category(){
    	$result = DB::table('tbl_category')->get();
    	return view('admin.pages.all_category')->with('category',$result);
    }


    public function edit_category($cate_id){
    	$result = DB::table('tbl_category')->where('category_id',$cate_id)->get();
    	return view('admin.pages.edit_category')->with('category',$result);
    }

    public function update_category(Request $request){
    	$data = array();
    	$data['category_name']= $request->category_name;
    	$data['category_content']= $request->category_content;
    	$category_id= $request->category_id;
    	$result = DB::table('tbl_category')->where('category_id',$category_id)->update($data);
    	$Session::put('message','Sửa danh mục sản phẩm thành công ');
    	return Redirect::to('/all-category');
    }

    public function delete_category($cate_id){
    	$result = DB::table('tbl_category')->where('category_id',$cate_id)->delete();
    	Session::put('message','Xóa sản phẩm thành công ');
    	return Redirect::to('/all-category');
    }

}
