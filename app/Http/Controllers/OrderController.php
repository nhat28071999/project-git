<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
session_start();

class OrderController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }

    public function show_order(){
    	$result = DB::table('tbl_order')->join('user','tbl_order.user_id','=','user.user_id')->select('tbl_order.order_id','tbl_order.order_total','tbl_order.order_status','user.user_name')->orderby('tbl_order.order_id','desc')->get();

    	return view('admin.pages.all_order')->with('menu',$result);
    }
    public function show_chitiet($order_id){
    	$result = DB::table('tbl_order')->join('user','tbl_order.user_id','=','user.user_id')->join('tbl_shipping','tbl_order.shipping_id','=','tbl_shipping.shipping_id')->join('tbl_order_detail','tbl_order.order_id','=','tbl_order_detail.order_id')->select('tbl_order.*','user.*','tbl_shipping.*','tbl_order_detail.*')->where('tbl_order.order_id',$order_id)->first();

    $detail = DB::table('tbl_order_detail')->join('tbl_order','tbl_order_detail.order_id','=','tbl_order.order_id')->where('tbl_order_detail.order_id',$order_id)->get();	
    	return view('admin.pages.show_chitiet')->with('order_by_id',$result)->with('detail',$detail);
    }
}
