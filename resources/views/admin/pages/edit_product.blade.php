@extends('admin.layout.app')
@section('content')
<div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                           Sửa  sản phẩm
                        </header>
                         <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                            ?>
                        <div class="panel-body">

                            <div class="position-center">
                                <form role="form" action="{{URL::to('/update-product')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    @foreach($product as $key => $pro)
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên sản phẩm</label>
                                    <input type="text" data-validation="length" data-validation-length="min10" value="{{$pro->product_name}}" name="product_name" class="form-control " id="slug" placeholder="Tên danh mục" onkeyup="ChangeToSlug();"> 
                                </div>
                                     <div class="form-group">
                                    <label for="exampleInputEmail1">Giá sản phẩm</label>
                                    <input type="text" data-validation="number" value="{{$pro->product_price}}" name="product_price" class="form-control" id="" placeholder="Tên danh mục">
                                    <input type="hidden" id="custId" name="product_id" value="{{$pro->product_id}}">
                                </div>
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Hình ảnh sản phẩm</label>
                                    <input type="file" name="product_image" class="form-control" id="exampleInputEmail1">
                                     <td><img src="{{URL::to('upload/product/'.$pro->product_image)}}" alt="" width="100px" height="100px"></td>
                                     <input type="hidden" id="custId" name="product_text_image" value="{{$pro->product_image}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Mô tả sản phẩm</label>
                                    <textarea style="resize: none"  rows="8" class="form-control" name="product_desc" id="ckeditor1" placeholder="Mô tả sản phẩm">{{$pro->product_desc}}</textarea>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputPassword1">Nội dung sản phẩm</label>
                                     <textarea id="summernote" name="product_content" rows="8">{{$pro->product_content}}</textarea>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputPassword1">Danh mục sản phẩm</label>
                                      <select name="category_id" class="form-control input-sm m-bot15">
                                            @foreach($category as $key => $cate)
                                            @if($pro->category_id == $cate->category_id)
                                            <option selected value="{{$pro->category_id}} "> {{$cate->category_name}} </option>
                                            @else
                                            <option  value="{{$pro->category_id}} "> {{$cate->category_name}} </option>
                                            @endif
                                            @endforeach
                                    </select>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputPassword1">Thương hiệu</label>
                                      <select name="brand_id" class="form-control input-sm m-bot15">
                                              @foreach($brand as $key => $bra)
                                            @if($pro->brand_id == $bra->brand_id)
                                            <option selected value="{{$pro->brand_id}} "> {{$bra->brand_name}} </option>
                                            @else
                                            <option  value="{{$pro->brand_id}} "> {{$bra->brand_name}} </option>
                                            @endif
                                            @endforeach
                                            
                                    </select>
                                </div>
                                @endforeach
                                <button type="submit" name="add_product" class="btn btn-info">Thêm sản phẩm</button>
                                </form>
                            </div>

                        </div>
                    </section>
                    <script>
                        $(document).ready(function() {
                            $('#summernote').summernote({
                                height: '120'
                            });
                        });
                      </script>
            </div>
@endsection