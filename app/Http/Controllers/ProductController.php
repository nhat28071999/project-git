<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManager;
use Image;
session_start();

class ProductController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }

    public function add_product(){
    	$category = DB::table('tbl_category')->orderby('id','desc')->get();
    	$brand = DB::table('tbl_brand')->orderby('brand_id','desc')->get();
    	return view('admin.pages.add_product')->with('category',$category)->with('brand',$brand);
    }

    public function save_product(Request $request){
    	$image1 = $request->file('product_image');
    	$image2 = $image1->getClientOriginalName();
    	Image::make($image1)->resize(300, 200)->save('upload/product/'.$image2);
    	
    	$data = array();
    	$data['product_name']= $request->product_name;
    	$data['product_price']= $request->product_price;
    	$data['product_desc']= $request->product_desc;
    	$data['product_content']= $request->product_content;
    	$data['product_image']=$image2;
    	$data['category_id']= $request->category_id;
    	$data['brand_id']= $request->brand_id;
    	
    	$result = DB::table('tbl_product')->insert($data);
    	Session::put('message','Thêm sản phẩm thành công');
    	return Redirect::to('/add-product');
    }

    public function all_product(){
        $result = DB::table('tbl_product')->join('tbl_category','tbl_product.category_id','=','tbl_category.category_id')->join('tbl_brand','tbl_product.brand_id','=','tbl_brand.brand_id')->select('tbl_product.product_name','tbl_product.product_price','tbl_product.product_image','tbl_product.product_id','tbl_brand.brand_name','tbl_category.category_name')->orderby('tbl_product.product_id','desc')->get();
        return view('admin.pages.all_product')->with('product',$result);
    }

    public function edit_product($product_id){
         $category = DB::table('tbl_category')->orderby('category_id','desc')->get();
         $brand = DB::table('tbl_brand')->orderby('brand_id','desc')->get();
         $product = DB::table('tbl_product')->where('product_id',$product_id)->get();
        return view('admin.pages.edit_product')->with('category',$category)->with('brand',$brand)->with('product',$product);
    }

    public function update_product(Request $request){
        
        $image1 = $request->file('product_image');
        if($image1) {
             $image2 = $image1->getClientOriginalName();
             Image::make($image1)->resize(300, 200)->save('upload/product/'.$image2);
        }else{
            $image2 = $request->product_text_image;
        }
      
        
        $product_id = $request->product_id;
        $data = array();
        $data['product_name']= $request->product_name;
        $data['product_price']= $request->product_price;
        $data['product_desc']= $request->product_desc;
        $data['product_content']= $request->product_content;
        $data['product_image']=$image2;
        $data['category_id']= $request->category_id;
        $data['brand_id']= $request->brand_id;
        
        $result = DB::table('tbl_product')->where('product_id',$product_id)->update($data);
        Session::put('message','Sửa sản phẩm thành công');
        return Redirect::to('/all-product');
    }

    public function delete_product($product_id){
        $resut = DB::table('tbl_product')->where('product_id',$product_id)->delete();
         Session::put('message','Xóa sản phẩm thành công');
        return Redirect::to('/all-product');
    }

}
