@extends('admin.layout.app')
@section('content')
        <div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Thêm Thương Hiệu Sản Phẩm
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <form role="form" method="post" action="{{URL::to('/save-brand')}}">
                                     @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên Thương Hiẹu:</label>
                                    <input type="text" class="form-control" name="brand_name" id="exampleInputEmail1" placeholder="Tên danh mục">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Mô tả Thương hiệu</label>
                                    <textarea style="resize: none;" rows="5" class="form-control" id="exampleInputPassword1" placeholder="Thêm mô tả" name="brand_content"></textarea>
                                </div>
                                <button type="submit" name="add_category_product" class="btn btn-info">Thêm thương hiệu</button><br>
                                <?php
                                $message = Session::get('message');
                                if($message){
                                    echo '<span class="text-alert" style="color:red;">'.$message.'</span>';
                                    Session::put('message',null);
                                }
                                ?>
                            </form>
                            </div>

                        </div>
                    </section>

            </div>
@endsection