<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class coupon extends Model
{
    public $timestamps = false;
    protected $fillable = [
          'coupon_name',  'coupon_code','coupon_number','coupon_info','coupon_write'
    ];
 
    protected $primaryKey = 'coupon_id';
 	protected $table = 'tl_coupon';
}
