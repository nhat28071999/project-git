<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
session_start();


class IndexController extends Controller
{
    public function index(/*sRequests $requet*/){
       /* $meta_desc = "Chuyên hàng điện tử và phụ kiện";
        $meta_keywords = "Hàng điện tử,Điện thoại,Máy tính";
        $meta_title = "Hàng điện tử";

        $meta_canonical = $request->url();*/


    	$category = DB::table('tbl_category')->orderby('category_id','desc')->get();
    	$brand =DB::table('tbl_brand')->orderby('brand_id','desc')->get();

    	$product = DB::table('tbl_product')->orderby('product_id','desc')->limit(6)->get();

        $random_product = DB::table('tbl_product')->take(3)->orderByRaw("RAND()")->get();

    	return view('client.pages.home')->with('category',$category)->with('brand',$brand)->with('product',$product)->with('randompro',$random_product);
        return view('client.pages.home')->with('category',$category)->with('brand',$brand)->with('product',$product)->with('randompro',$random_product);
    }

    public function category($category_id){
    	$category = DB::table('tbl_category')->orderby('category_id','desc')->get();
    	$brand =DB::table('tbl_brand')->orderby('brand_id','desc')->get();

    	$product = DB::table('tbl_product')->where('category_id',$category_id)->get();

        
    	return view('client.pages.show_category')->with('category',$category)->with('brand',$brand)->with('product',$product);
    }

    public function show_brand($brand_id){
            $category = DB::table('tbl_category')->orderby('category_id','desc')->get();
            $brand =DB::table('tbl_brand')->orderby('brand_id','desc')->get();
            $title = DB::table('tbl_brand')->where('brand_id',$brand_id)->get();
            $product = DB::table('tbl_product')->where('brand_id',$brand_id)->get();

        
        return view('client.pages.show_brand')->with('category',$category)->with('brand',$brand)->with('title',$title)->with('product',$product);
    }

    public function search_product(Request $request){
        $search = $request->search_product;

        $result = DB::table('tbl_product')->where('product_name','like','%'.$search.'%')->get();     
   
      $category = DB::table('tbl_category')->orderby('category_id','desc')->get();
        $brand =DB::table('tbl_brand')->orderby('brand_id','desc')->get();


        return view('client.pages.show_search')->with('category',$category)->with('brand',$brand)->with('product',$result);

    }

    public function show_product($product_id){
        $category = DB::table('tbl_category')->orderby('category_id','desc')->get();
        $brand =DB::table('tbl_brand')->orderby('brand_id','desc')->get();

        $product = DB::table('tbl_product')->join('tbl_category','tbl_product.category_id','=','tbl_category.category_id')->join('tbl_brand','tbl_product.brand_id','=','tbl_brand.brand_id')->select('tbl_product.*','tbl_category.category_name','tbl_brand.brand_name')->where('product_id',$product_id)->get();

        foreach ($product as $key => $value) {
            $categoryone = $value->category_id;
        }

        $product_like = DB::table('tbl_product')->join('tbl_category','tbl_product.category_id','=','tbl_category.category_id')->join('tbl_brand','tbl_product.brand_id','=','tbl_brand.brand_id')->where('tbl_product.category_id',$categoryone )->whereNotIn('tbl_product.product_id',[$product_id])->get();



        return view('client.pages.show_product')->with('category',$category)->with('brand',$brand)->with('product',$product)->with('productone',$product_like);
    }
}


