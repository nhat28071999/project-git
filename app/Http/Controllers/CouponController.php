<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\coupon;
session_start();

class CouponController extends Controller
{
    public function add_coupon(){
    	return view('admin.pages.add_coupon');
    }
    public function all_coupon(){
    	$coupon = coupon::orderby('coupon_id','desc')->get();
    	return view('admin.pages.all_coupon')->with(compact('coupon'));
    }
    public function save_coupon(Request $request){
    	$data = $request->all();
    	$coupon = new coupon;

    	$coupon->coupon_name = $data['coupon_name'];
    	$coupon->coupon_code =$data['coupon_code'];
    	$coupon->coupon_number =$data['coupon_number'];
    	$coupon->coupon_info =$data['coupon_info'];
    	$coupon->coupon_write =$data['coupon_write'];
    	$coupon->save();

    	Session::put('message','Thêm mã giảm giá thành công');
    	return Redirect::to('/add-coupon');
    }
    public function delete_coupon($coupon_id){
    	$coupon = coupon::find($coupon_id)->delete();
    	Session::put('message','Xóa mã thành công');
    	return Redirect::to('/all-coupon');
    }
}
