@extends('client.layout.cart_app')
@section('content')
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Đăng nhập tài khoản</h2>
						<form action="{{URL::to('/login-customer')}}" method="POST">
							{{ csrf_field() }}
							<input type="text" placeholder="Name" name="email_accout" />
							<input type="password" name="password_accout" placeholder="Password" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Nhớ mật khẩu
							</span>
							<button type="submit" class="btn btn-default">Login</button>
							<?php
                                $done1 = Session::get('done1');
                                if($done1){
                                    echo '<span class="text-alert" style="color:red;">'.$done1.'</span>';
                                    Session::put('done',null);
                                }
                                ?>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-5">
					<div class="signup-form"><!--sign up form-->
						<h2>Đăng ký</h2>
						<form action="{{URL::to('/add-customer')}}" method="POST">
							{{csrf_field() }}
							<input type="text" placeholder="Tên" name="customer_name">
							@error('customer_name')
							    <div class="alert alert-danger">{{ $message }}</div>
							@enderror
							<input type="email" placeholder="Địa chỉ email" name="customer_email"/>
							@error('customer_email')
							    <div class="alert alert-danger">{{ $message }}</div>
							@enderror
							<input type="password" placeholder="Mật khẩu" name="customer_password"/>
							@error('customer_password')
							    <div class="alert alert-danger">{{ $message }}</div>
							@enderror
							<input type="text" placeholder="Số điện thoại" name="customer_phone"/>
							@error('customer_phone')
							    <div class="alert alert-danger">{{ $message }}</div>
							@enderror
							 <div class="g-recaptcha" data-sitekey="6LdsnYQaAAAAAE92VgUa4l1ICKhsZ-W9iO7Uc2HE"></div>
								<br/>
								@if($errors->has('g-recaptcha-response'))
								<span class="invalid-feedback" style="display:block">
									<div class="alert alert-danger">{{$errors->first('g-recaptcha-response')}}</div>
								</span>
								@endif

							<button type="submit" class="btn btn-default">Signup</button>
							   <?php
                                $done = Session::get('done');
                                if($done){
                                    echo '<span class="text-alert" style="color:red;">'.$done.'</span>';
                                    Session::put('done',null);
                                }
                                ?>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
		
	</section><!--/form-->
@endsection