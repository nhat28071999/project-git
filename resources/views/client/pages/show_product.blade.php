@extends('client.layout.app')
@section('content')
@foreach($product as $key => $value )
<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="..\upload\product\{{$value->product_image}}" alt="" />
								<h3>ZOOM</h3>
							</div>
							{{-- <div id="similar-product" class="carousel slide" data-ride="carousel">
								
								  <!-- Wrapper for slides -->
								    <div class="carousel-inner">
										<div class="item active">
										  <a href=""><img src="{{URL::to('public/frontend/images/similar1.jpg')}}" alt=""></a>
										  <a href=""><img src="{{URL::to('public/frontend/images/similar1.jpg')}}" alt=""></a>
										  <a href=""><img src="{{URL::to('public/frontend/images/similar1.jpg')}}" alt=""></a>
										</div>
										<div class="item">
										  <a href=""><img src="{{URL::to('public/frontend/images/similar1.jpg')}}" alt=""></a>
										  <a href=""><img src="{{URL::to('public/frontend/images/similar1.jpg')}}" alt=""></a>
										  <a href=""><img src="{{URL::to('public/frontend/images/similar1.jpg')}}" alt=""></a>
										</div>
										<div class="item">
										  <a href=""><img src="{{URL::to('public/frontend/images/similar1.jpg')}}" alt=""></a>
										  <a href=""><img src="{{URL::to('public/frontend/images/similar1.jpg')}}" alt=""></a>
										  <a href=""><img src="{{URL::to('public/frontend/images/similar1.jpg')}}" alt=""></a>
										</div>
										
									</div>

								  <!-- Controls -->
								  <a class="left item-control" href="#similar-product" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								  </a>
								  <a class="right item-control" href="#similar-product" data-slide="next">
									<i class="fa fa-angle-right"></i>
								  </a>
							</div> --}}

						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<img src="images/product-details/new.jpg" class="newarrival" alt="" />
								<h2>{{ $value->product_name}}</h2>
								<p>Mã sản phẩm: {{ $value->product_id }}</p>
								<img src="images/product-details/rating.png" alt="" />
								<form action="{{ URL::to('/save-cart')}}" method="POST">
									{{csrf_field() }}
								<span>
									<span>{{ number_format($value->product_price).'VND'}}</span>
									<label>Số Lượng:</label>
									<input type="number" name="qly" min="1" value="1" />
									<input type="hidden" name="product_hidden" value="{{ $value->product_id }}" />

									<button type="submit" class="btn btn-fefault cart">
										<i class="fa fa-shopping-cart"></i>
										Thêm vào giỏ hàng
									</button>
									</form>
								</span>
								<p><b>Khả dụng:</b> In Stock</p>
								<p><b>Tình trạng:</b> New</p>
								<p><b>Thương  hiệu:</b>{{ $value->brand_name}}</p>
								<p><b>Danh mục:</b>{{ $value->category_name}}</p>
								<a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
@endforeach
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12" style=" background-color: black;">
							<ul class="nav nav-tabs" style="margin-bottom: 0px; background-color: black; ">
								<li class="active"><a href="#details" data-toggle="tab">Chi tiết</a></li>
								<li><a href="#companyprofile" data-toggle="tab">Công ty</a></li>
								<li><a href="#tag" data-toggle="tab">Tag</a></li>
								<li ><a href="#reviews" data-toggle="tab">Đánh giá</a></li>
							</ul>
						</div>
						<div class="tab-content" >
							<div class="tab-pane fade active in" id="details" >
								<div class="col-sm-12">
									<pre style="background-color: white;"><?php echo $value->product_content; ?></pre>
								</div>
								
							</div>
							
							<div class="tab-pane fade" id="companyprofile" >
								<div class="col-sm-5">
									<h4>Công ty : {{ $value->brand_name }} Tại việt nam</h4>
								</div>
								
								
							</div>
							
							<div class="tab-pane fade " id="reviews" >
								<div class="col-sm-12">
									<div class="fb-comments" data-href="http://localhost/blog/public/show-product/{{ $value->product_id }}" data-width="" data-numposts="5"></div>
								</div>
							</div>
							
						</div>
					</div><!--/category-tab-->
				
					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title text-center">Sản phẩm liên quan</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								@foreach($productone as $key => $lienquan)
								<div class="item active">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="..\upload\product\{{$lienquan->product_image}}" style=" width :190px; height: 140px;" alt="" />
													<h2>{{ number_format($lienquan->product_price).'VND'}}</h2>
													<p>{{ $lienquan->product_name}}</p>
													<button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
												</div>
											</div>
										</div>
									</div>
									
									
								</div>
								@endforeach
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div><!--/recommended_items--> 
@endsection