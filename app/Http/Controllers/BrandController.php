<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
session_start();

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add_brand(){
    	return view('admin.pages.add_brand');
    }

    public function save_brand(Request $request){
    	$data = array();
    	$data['brand_name'] = $request->brand_name;
    	$data['brand_content'] = $request->brand_content;

    	$result = DB::table('tbl_brand')->insert($data);
    	Session::put('message','Thêm danh mục sản phẩm thành công');
    	return Redirect::to('/add-brand');
    }

    public function all_brand(){

    	$result = DB::table('tbl_brand')->get();
    	return view('admin.pages.all_brand')->with('brand',$result);
    }

   public function edit_brand($brand_id){
        $result = DB::table('tbl_brand')->where('brand_id',$brand_id)->get();
        return view('admin.pages.edit_brand')->with('brand',$result);
    }

    public function update_brand(Request $request){
        $data = array();
        $data['brand_name'] = $request->brand_name;
        $data['brand_content'] = $request->brand_content;
        $brand_id = $request->brand_id;
        $result = DB::table('tbl_brand')->where('brand_id',$brand_id)->update($data);
        Session::put('message','Sửa thương hiệu thành công');
        return Redirect::to('/all-brand');
    }

    public function delete_brand( $brand_id){
        $result = DB::table('tbl_brand')->where('brand_id',$brand_id)->delete();
        Session::put('message','Xóa thương hiệu thành công ');
        return Redirect::to('/all-brand');
    }
}
