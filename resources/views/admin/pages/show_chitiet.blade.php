@extends('admin.layout.app')
@section('content')
  
        <div class="table-agile-info">
  <div class="panel panel-default">
    <div class="panel-heading">
      Thông tin người đặt hàng 
    </div>
    
    <div class="table-responsive">
     
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th style="width:20px;">
              <label class="i-checks m-b-none">
                <input type="checkbox"><i></i>
              </label>
            </th>
            <th>Tên người đặt</th>
            <th>Số điện thoại</th>
            <th>Email </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
            <td>{{$order_by_id->user_name}}</td>
            <td>{{$order_by_id->user_phone}}</td>
            <td>{{$order_by_id->user_email}}</td>
          </tr>      
        </tbody>
      </table>
    </div>  
  </div>
</div>

<br><br>
<div class="table-agile-info">
  <div class="panel panel-default">
    <div class="panel-heading">
      Thông tin vận chuyển
    </div>
    
    <div class="table-responsive">
     
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th style="width:20px;">
              <label class="i-checks m-b-none">
                <input type="checkbox"><i></i>
              </label>
            </th>
            <th>Tên người vận chuyển</th>
            <th>Địa chỉ</th>
            <th>Email</th>
            <th>Số điện thoại</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
            <td>{{$order_by_id->shipping_name}}</td>
            <td>{{$order_by_id->shipping_address}}</td>
            <td>{{$order_by_id->shipping_email}}</td>
            <td>{{$order_by_id->shipping_phone}}</td>
          </tr>      
        </tbody>
      </table>
    </div>  
  </div>
</div>
<br><br>
<div class="table-agile-info">
  <div class="panel panel-default">
    <div class="panel-heading">
      Chi tiết đơn hàng 
    </div>
    
    <div class="table-responsive">
     
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th style="width:20px;">
              <label class="i-checks m-b-none">
                <input type="checkbox"><i></i>
              </label>
            </th>
            <th>Tên sản phẩm</th>
            <th>Số lượng </th>
            <th>Gía</th>
            <th>Tổng tiền</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
            @foreach($order_by_id as $content)
            <td>{{$content->product_name}}</td>
            <td>{{$content->product_sales_quantity}}</td>
            <td>{{$content->product_price}}</td>
            <td>{{$content->order_total}}</td>
            @endforeach
          </tr>      
        </tbody>
      </table>
    </div>  
  </div>
</div>


@endsection